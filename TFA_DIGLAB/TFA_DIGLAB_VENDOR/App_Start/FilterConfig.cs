﻿using System.Web;
using System.Web.Mvc;

namespace TFA_DIGLAB_VENDOR
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
