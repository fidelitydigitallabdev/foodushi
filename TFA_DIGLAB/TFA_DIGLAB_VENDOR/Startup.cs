﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TFA_DIGLAB_VENDOR.Startup))]
namespace TFA_DIGLAB_VENDOR
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
